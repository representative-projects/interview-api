<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionReplyOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_reply_options', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger( 'question_id' )->index();
            $table->string( 'option_value', 50 )->nullable( true );
            $table->timestamps();
            $table->foreign( 'question_id' )->references( 'id' )->on( 'questions' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question_reply_options');
    }
}
