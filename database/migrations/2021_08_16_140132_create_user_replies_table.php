<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserRepliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_replies', function (Blueprint $table) {
            $table->id();

            $table
                ->unsignedBigInteger( 'user_id' )
                ->index();
            $table
                ->unsignedBigInteger( 'question_reply_option_id' )
                ->index();

            $table->string('interview_uid', 13)->index();
            $table->text( 'value' );
            $table->timestamps();

            $table
                ->foreign( 'user_id' )
                ->references( 'id' )
                ->on( 'users' );
            $table
                ->foreign( 'question_reply_option_id' )
                ->references( 'id' )
                ->on( 'question_reply_options' );
            $table
                ->foreign( 'interview_uid' )
                ->references( 'interview_uid' )
                ->on( 'user_interviews' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_replies');
    }
}
