<?php

namespace Database\Seeders;

use App\Services\DatabaseSeederService;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    const USER_COUNT                        = 50;
    const USER_INTERVIEW_COUNT              = 100000;
    const DB_INSERT_AFTER_CYCLE             = 1000;

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $bdPopulationService = new DatabaseSeederService(
            self::USER_COUNT,
            self::USER_INTERVIEW_COUNT,
            self::DB_INSERT_AFTER_CYCLE
        );
        $bdPopulationService->populateDbSQL();
    }
}
