<?php

namespace Database\Factories;

use App\Models\QuestionReplyOption;
use Illuminate\Database\Eloquent\Factories\Factory;

class QuestionReplyOptionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = QuestionReplyOption::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'option_value'            => $this->faker->numberBetween( 0, 5 )
        ];
    }
}
