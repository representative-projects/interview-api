<?php

namespace Database\Factories;

use App\Models\UserInterview;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserInterviewFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = UserInterview::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
