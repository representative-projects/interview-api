<?php

namespace Database\Factories;

use App\Models\UserReplies;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserRepliesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = UserReplies::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
