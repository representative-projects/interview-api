<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\QuestionReplyOption;
use App\Models\User;
use App\Models\UserInterview;
use App\Models\UserReplies;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class ApiController extends Controller
{
    /**
     * Returns a json listing of the resource.
     *
     */
    public function index()
    {
        return UserInterview::apiInterviewCollection();
    }

    /**
     * Store a newly created User Interview in storage.
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function store( Request $request ): JsonResponse
    {
        $this->validate( $request, [
            'user_id'           => 'required|integer',
            'replies'           => 'array',
        ] );

        $params                 = $request->all();
        $client                 = User::findOrFail( $params[ 'user_id' ] );
        $interviewUid           = uniqid();
        $userReplies            = [];

        foreach ( $params[ 'replies' ] as $reply ) {
            if ( isset( $reply[ 'question_reply_option_id' ] ) ) {
                $replyOption            = QuestionReplyOption::findOrFail( $reply[ 'question_reply_option_id' ] );

                $replyOptionValue       = $replyOption->option_value;
                if ( $replyOption->question->question_type === 'open' ) {
                    $replyOptionValue   = $reply[ 'value' ];
                }
                $userReplies[]                      = [
                    'value'                         => $replyOptionValue,
                    'user_id'                       => $client->id,
                    'interview_uid'                 => $interviewUid,
                    'question_reply_option_id'      => $replyOption->id,
                ];
            }
        }

        UserInterview::factory()->create( [ 'interview_uid' => $interviewUid, 'user_id' => $client->id ] );
        UserReplies::factory()->createMany( $userReplies );

        return response()->json( [ 'result' => 'Reply is created' ], 201 );
    }
}
