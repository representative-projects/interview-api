<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\Statistics;

class ApiStatistics extends Controller
{

    /**
     * Returns User Replies statistics.
     *
     */
    public function getStatistics()
    {
        $statistics = new Statistics;

        return response()->json( $statistics() );
    }
}
