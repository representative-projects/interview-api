<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Question as QuestionResource;

class QuestionReplyOption extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'reply_option_id'       => $this->id,
            'option_value'          => $this->option_value,
            'question'              => QuestionResource::collection( $this->question()->get() )
        ];
    }
}
