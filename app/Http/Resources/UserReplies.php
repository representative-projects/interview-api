<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserReplies extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'reply_id'              => $this->id,
            'value'                 => $this->value,
            'created'               => $this->created_at,
            'reply_option'          => QuestionReplyOption::collection( $this->questionReplyOption()->get() )
        ];
    }
}
