<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\User as UserResource;
use App\Http\Resources\UserReplies as UserRepliesResource;

class UserInterview extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'interview_id'          => $this->id,
            'interview_uid'         => $this->interview_uid,
            'user'                  => UserResource::collection( $this->user()->get() ),
            'user_replies'          => UserRepliesResource::collection( $this->userReplies()->get() ),
        ];
    }
}
