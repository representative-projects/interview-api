<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserReplies extends Model
{
    use HasFactory;

    public function user()
    {
        return $this->belongsTo( User::class );
    }

    public function questionReplyOption()
    {
        return $this->belongsTo( QuestionReplyOption::class );
    }

    public function userInterview()
    {
        return $this->belongsTo( UserInterview::class, 'interview_uid', 'interview_uid');
    }

    public static function getRepliesTotalCount()
    {
        return self::all()->count();
    }

    public static function getScalarRepliesTotalCount()
    {
        return self::getScalarRepliesResource()->count();
    }

    public static function getOpenRepliesTotalCount()
    {
        return self::getOpenRepliesResource()->count();
    }

    public static function getScalarRepliesResource()
    {
        return self::whereIn( 'question_reply_option_id', QuestionReplyOption::getScalarQuestionsRepliesOptionsModelKeys() )->get();
    }

    public static function getOpenRepliesResource()
    {
        return self::whereIn( 'question_reply_option_id', QuestionReplyOption::getOpenQuestionsRepliesOptionModelKeys() )->get();
    }
}
