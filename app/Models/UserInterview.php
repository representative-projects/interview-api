<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\UserInterview as UserInterviewResource;

class UserInterview extends Model
{
    use HasFactory;

    public function user()
    {
        return $this->belongsTo( User::class );
    }

    public function userReplies()
    {
        return $this->hasMany( UserReplies::class, 'interview_uid', 'interview_uid' );
    }

    public static function apiInterviewCollection()
    {
        $userInterviews = UserInterview::orderBy('id', 'desc')->paginate( 10 );

        return UserInterviewResource::collection( $userInterviews );
    }
}
