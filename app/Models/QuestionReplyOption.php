<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuestionReplyOption extends Model
{
    use HasFactory;

    public function question()
    {
        return $this->belongsTo( Question::class );
    }

    public function userReplies()
    {
        return $this->hasMany( UserReplies::class );
    }

    public static function getScalarQuestionsRepliesOptionsModelKeys()
    {
        return self::on()->whereIn( 'question_id', Question::getScalarQuestionModelKeys() )->get()->modelKeys();
    }

    public static function getOpenQuestionsRepliesOptionModelKeys()
    {
        return self::on()->whereIn( 'question_id', Question::getOpenQuestionModelKeys() )->get()->modelKeys();
    }

    public static function getQuestionRepliesOptionsModelKeys( $questionId )
    {
        return self::on()->where( 'question_id', $questionId )->get()->modelKeys();
    }
}
