<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Question extends Model
{
    use HasFactory;

    public function questionReplyOption()
    {
        return $this->hasMany( QuestionReplyOption::class );
    }

    public static function getScalarQuestionModelKeys()
    {
        return self::on()->where('question_type', '=', 'scalar' )->get()->modelKeys();
    }

    public static function getOpenQuestionModelKeys()
    {
        return self::on()->where('question_type', '=', 'open' )->get()->modelKeys();
    }

    public static function getQuestionModelKeys()
    {
        return self::all()->modelKeys();
    }

    public static function getScalarQuestionResource()
    {
        return self::on()->where( 'question_type', 'scalar' )->get();
    }

    public static function getInterviewStatisticResource( $questionId )
    {
        return DB::table( 'questions', 'q' )
            ->leftJoin( 'question_reply_options', 'q.id', '=', 'question_reply_options.question_id' )
            ->leftJoin( 'user_replies', 'question_reply_options.id', '=', 'user_replies.question_reply_option_id' )
            ->select( [
                'question_reply_options.id as option_id',
                'question_reply_options.option_value',
                DB::raw( 'COUNT(user_replies.question_reply_option_id) as reply_count' ),
                ] )
            ->where( 'q.id', '=', $questionId )
            ->groupBy( 'option_id' )
            ->get();
    }
}
