<?php

namespace App\Services;

use App\Models\Question;
use App\Models\QuestionReplyOption;
use App\Models\User;
use App\Models\UserInterview;
use App\Models\UserReplies;
use Faker\Generator;
use Illuminate\Container\Container;
use Illuminate\Support\Facades\DB;

class DatabaseSeederService
{
    const USER_COUNT = 1;
    const USER_INTERVIEW_COUNT = 1;
    const DB_INSERT_AFTER_CYCLE = 1;

    private $faker;

    private $usersCount;
    private $usersInterviewCount;
    private $insertAfterCycle;

    public function __construct( $usersCount = null, $interviewCount = null, $insertAfterCycle = null)
    {
        if ( $interviewCount !== null && $insertAfterCycle !== null && $usersCount !== null ) {
            $this->usersCount                           = $usersCount;
            $this->usersInterviewCount                  = $interviewCount;
            $this->insertAfterCycle                     = $insertAfterCycle;
        } else {
            $this->usersCount                           = self::USER_COUNT;
            $this->usersInterviewCount                  = self::USER_INTERVIEW_COUNT;
            $this->insertAfterCycle                     = self::DB_INSERT_AFTER_CYCLE;
        }

        $this->faker = Container::getInstance()->make(Generator::class);
    }

    /**
     * Seed the application's database with big amount of data.
     *
     * @return void
     */
    public function populateDbSQL()
    {
        $this->populateUsers();

        $this->populateQuestions();

        $this->populateUserRepliesSQL();
    }

    /**
     * Seed the application's database with small amount of data.
     *
     * @return void
     */
    public function populateDbFactory()
    {
        $this->populateUsers();

        $this->populateQuestions();

        $this->populateUserRepliesWithFactory();
    }

    public function populateUsers()
    {
        User::factory()->count( $this->usersCount )->create();

        return $this;
    }

    public function populateQuestions()
    {
        Question::factory()->count( 9 )->create( [ 'question_type' => 'scalar' ] );
        Question::factory()->create();

        $this->populateQuestionReplyOptions();

        return $this;
    }

    private function populateQuestionReplyOptions()
    {
        Question::all()->each( function ( $question ) {
            switch ( $question->question_type ) {
                case 'scalar':
                    QuestionReplyOption::factory()->createMany( [
                        [ 'question_id' => $question->id, 'option_value' => 0 ],
                        [ 'question_id' => $question->id, 'option_value' => 1 ],
                        [ 'question_id' => $question->id, 'option_value' => 2 ],
                        [ 'question_id' => $question->id, 'option_value' => 3 ],
                        [ 'question_id' => $question->id, 'option_value' => 4 ],
                        [ 'question_id' => $question->id, 'option_value' => 5 ],
                    ] );
                    break;
                case 'open':
                    QuestionReplyOption::factory()->createOne( [ 'question_id' => $question, 'option_value' => ''] );
                    break;
            }
        } );
    }

    private function populateUserRepliesWithFactory()
    {
        DB::disableQueryLog();
        $q                          = 1;
        $users                      = User::all();
        $questions                  = Question::all();
        while ( $q <= $this->usersInterviewCount ) {
            $user                   = $users->random();
            $interviewUid           = uniqid();
            UserInterview::factory()->create( [ 'interview_uid' => $interviewUid, 'user_id' => $user->id ] );
            $questions->each( function ( $question ) use ( $user, $interviewUid ) {
                $replyOption            = $question->questionReplyOption()->getResults()->random();
                $optionValue            = $replyOption->option_value;
                if ( $question->question_type === 'open' ) {
                    $optionValue        = $this->faker->text( rand( 50, 200 ) );
                }
                UserReplies::factory()->create( [
                    'user_id' => $user->id,
                    'interview_uid' => $interviewUid,
                    'question_reply_option_id' => $replyOption->id,
                    'value' => $optionValue,
                ] );
            });
            $q++;
        }
    }

    private function populateUserRepliesSQL()
    {
        DB::disableQueryLog();
        $q                          = 1;
        $iq                         = 1;
        $users                      = User::all();
        $questions                  = Question::all();
        $userReplies                = [];
        while ( $q <= $this->usersInterviewCount ) {
            $i                      = 1;
            $user                   = $users->random();
            $interviewUid           = uniqid();
            $userId                 = $user->id;
            $userInterview[]        = [
                'user_id'           => '"' . $userId . '"',
                'interview_uid'     => '"' . $interviewUid . '"',
            ];
            while ( $i <= 10 ) {
                $question = $questions->find( [ 'id' => $i ] )->first();
                if ( $question !== null && $question->question_type === 'open' ) {
                    $userReplies[] = [
                        'user_id'                           => '"' . $userId . '"',
                        'interview_uid'                     => '"' . $interviewUid . '"',
                        'question_reply_option_id'          => '"' . $question->questionReplyOption()->getResults()->first()->id . '"',
                        'value'                             => '"' . $this->faker->text( rand( 50, 200 ) ) . '"',
                    ];
                } elseif ( $question !== null && $question->question_type === 'scalar' ) {
                    $replyOption = $question->questionReplyOption()->getResults()->random();
                    $userReplies[] = [
                        'user_id'                           => '"' . $userId . '"',
                        'interview_uid'                     => '"' . $interviewUid . '"',
                        'question_reply_option_id'          => '"' . $replyOption->id . '"',
                        'value'                             => '"' . $replyOption->option_value . '"',
                    ];
                }
                unset( $question, $replyOption );
                $i++;
            }

            if ( !( $iq % $this->insertAfterCycle ) ) {
                $this->insertUserInterviewSQL( $userInterview );
                $this->insertUserRepliesSQL( $userReplies );

                $userInterview      = [];
                $userReplies        = [];
                $iq = 0;
            }
            unset( $i, $user, $interviewUid );
            $iq++;
            $q++;
        }
    }

    private function insertUserInterviewSQL( $userInterview )
    {
        $sqlValues          = '';
        $interviewSQL       = 'INSERT INTO user_interviews (user_id, interview_uid) VALUES ';
        foreach ( $userInterview as $key => $values ) {
            $sqlValues      .= '(' . implode( ',', $values ) . ')';
            end( $userInterview );
            if ( $key !== key( $userInterview ) ) {
                $sqlValues  .= ', ';
            }
        }
        $interviewSQL       .= $sqlValues;
        DB::statement( $interviewSQL );
    }

    private function insertUserRepliesSQL( $userReplies )
    {

        $sqlValues          = '';
        $replySQL           = 'INSERT INTO user_replies (user_id, interview_uid, question_reply_option_id, value) VALUES ';
        foreach ( $userReplies as $key => $values ) {
            $sqlValues      .= '(' . implode( ',', $values ) . ')';
            end( $userReplies );
            if ( $key !== key( $userReplies ) ) {
                $sqlValues  .= ', ';
            }
        }
        $replySQL           .= $sqlValues;
        DB::statement( $replySQL );
    }
}
