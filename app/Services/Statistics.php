<?php

namespace App\Services;

use App\Models\Question;

class Statistics
{
    private $tempAverageData;
    private $answerPerQuestion;
    private $questionModelKeys;
    private $statisticsResource;
    private $questionAverageData;
    private $openQuestionModelKeys;
    private $scalarQuestionModelKeys;
    private $openQuestionTotalReplyCount = null;
    private $scalarQuestionTotalReplyCount = null;

    public function __construct()
    {
        $this->questionModelKeys = Question::getQuestionModelKeys();
        $this->openQuestionModelKeys = Question::getOpenQuestionModelKeys();
        $this->scalarQuestionModelKeys = Question::getScalarQuestionModelKeys();
    }
    public function __invoke()
    {
        $this->getStatisticsResource();

        $this->statisticsCalculation();

        return $this->getResponseData();
    }

    private function getStatisticsResource()
    {
        foreach ( $this->questionModelKeys as $questionId ) {
            $this->statisticsResource[ $questionId ] = Question::getInterviewStatisticResource( $questionId );
        }
    }

    private function statisticsCalculation()
    {
        foreach ($this->statisticsResource as $questionId => $collection ) {
            foreach ( $collection as $resource ) {
                $this->tempAverageData[ $questionId ][ $resource->option_value ] = $resource->reply_count;
                $this->answerPerQuestion[ $questionId ][] = [
                    'option_id' => $resource->option_id,
                    'option_value' => $resource->option_value,
                    'answer_count' => $resource->reply_count,
                ];
                if ( in_array( $questionId, $this->scalarQuestionModelKeys, true ) ) {
                    $this->scalarQuestionTotalReplyCount += $resource->reply_count;
                }
                if ( in_array( $questionId, $this->openQuestionModelKeys, true ) ) {
                    $this->openQuestionTotalReplyCount += $resource->reply_count;
                }
            }
            $mostAnswered = max( $this->tempAverageData[ $questionId ] );
            $totalReplyCount = array_sum( $this->tempAverageData[ $questionId ] );
            $this->questionAverageData[] = [
                'question_id' => $questionId,
                'average_option_value' => array_flip( $this->tempAverageData[ $questionId ])[ $mostAnswered ],
                'chosen' => $mostAnswered,
                'question_reply_total_count' => $totalReplyCount,
            ];
            $this->answerPerQuestion[ $questionId ][] = [
                'total_reply_count' => $totalReplyCount
            ];
        }
    }

    private function getResponseData()
    {
        return [
            'graph_questions'           => [
                'answers_count'         => $this->scalarQuestionTotalReplyCount,
                'question_average'      => $this->questionAverageData,
                'answers_per_question'  => $this->answerPerQuestion,
            ],
            'open_question'             => [
                'answers_count'         => $this->openQuestionTotalReplyCount,
            ]
        ];
    }
}
