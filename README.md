# Interview Project


## Setup Process
___

- Clone/Download repository
- Install project via `composer`
- Populate DB via `php artisan migrate --seed`, wait until it completes(~10min)
- Run the server via `php artisan serve`

## API's
___

| URL               | METHOD        | PURPOSE                           |
| -------------     |:-------------:| -------------                     |
| /api/stats        | GET           | Gets common interviews statistics |
| /api/questions    | GET           | Gets last 10 interviews           |
| /api/questions    | POST          | Creates new interview             |

## Requests
___

- GET /api/questions?page=
```http request
page=int  - optional
```

- POST /api/questions
```http request
{
    "user_id": int,
    "replies": [
        {
            "question_reply_option_id": int,
            "value": int|text
        },
        ...
    ]
}
```

- GET /api/stats
```http request

```

## Responses
___

- GET /api/questions
```json
{
    "data": [
        {
            "interview_id": int,
            "interview_uid": string,
            "user": [
                {
                    "user_id": int,
                    "name": string,
                    "email": email
                }
            ],
            "user_replies": [
                {
                    "reply_id": int,
                    "value": string,
                    "created": string|null,
                    "reply_option": [
                        {
                            "reply_option_id": int,
                            "option_value": sting,
                            "question": [
                                {
                                    "question_id": int,
                                    "question": string,
                                    "question_type": string
                                }
                            ]
                        }
                    ]
                },
                ...
            ]
        }
    ],
    "links": {
        "first": "{HOST}/api/questions?page=1",
        "last": string|null,
        "prev": string|null,
        "next": "{HOST}/api/questions?page=2"
    },
    "meta": {
        "current_page": int,
        "from": int,
        "last_page": int,
        "links": [
            {
                "url": null,
                "label": "&laquo; Previous",
                "active": false
            },
            {
                "url": "{HOST}/api/questions?page=1",
                "label": "1",
                "active": true
            },
            ...
        ],
        "path": "{HOST}/api/questions",
        "per_page": int,
        "to": int,
        "total": int
    }
}
```

- POST /api/questions
```json
{
    "result": "Reply is created"
}
```

- GET /api/stats
```json
{
    "graph_questions": {
        "question_average": [
            {
                "question_id": int,
                "average_option_value": int,
                "chosen": int,
                "question_reply_total_count": int
            },
            ...
        ],
        "answers_count": int,
        "answers_per_question": [
            {
                (question_id) int: [
                    {
                        "option_id": int,
                        "option_value": int,
                        "answers_count": int
                    },
                    ...
                ],
            }
        ]
    },
    "open_question": {
        "answers_count": int,
    }
}
```

## Tests
___

```shell
./vendor/bin/phpunit
```
