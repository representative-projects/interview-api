<?php

namespace Tests;

use App\Models\User;
use App\Services\DatabaseSeederService;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function user()
    {
        return User::factory()->create();
    }

    public function seed( $usersCount = 1, $interviewCount = 1, $insertAfterCycle = 1 )
    {
        return new DatabaseSeederService( $usersCount, $interviewCount, $insertAfterCycle );
    }
}
