<?php

namespace Tests\Feature;

use App\Models\Question;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DatabaseTest extends TestCase
{
    use RefreshDatabase,
        WithFaker;

    public function testDeleteUser()
    {
        $this->user();

        $user = User::all()->first();
        $user->delete();

        $this->assertDeleted( $user );
    }

    public function testTableHasAmountOfTheUsers()
    {
        $countOfUsers = 5;
        $this->seed( $countOfUsers )->populateUsers();

        $this->assertDatabaseCount( 'users', $countOfUsers);
    }

    public function testDbHasRequiredQuestionsTypes()
    {
        $this->seed()->populateQuestions();

        $this->assertDatabaseHas( 'questions', ['question_type' => 'scalar'] )
            ->assertDatabaseHas( 'questions', [ 'question_type' => 'open'] );
    }

    public function testOpenQuestionUpdate()
    {
        $this->seed()->populateQuestions();

        $this->assertDatabaseHas( 'questions', [ 'question_type' => 'open' ] );
        $this->assertDatabaseMissing( 'questions', [ 'question_type' => 'closed']);

        $question = Question::on()->where( 'question_type', '=', 'open')->get()->first();
        $question->question_type = 'closed';
        $question->save();

        $this->assertDatabaseHas( 'questions', [ 'question_type' => 'closed' ] );
        $this->assertDatabaseMissing( 'questions', [ 'question_type' => 'open']);
    }
}
