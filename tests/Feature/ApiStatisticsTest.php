<?php

namespace Tests\Feature;

use App\Models\Question;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;
use function PHPUnit\Framework\assertIsArray;

class ApiStatisticsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /**
     * Response status and structure test
     */
    public function testStatisticsApi()
    {
        $this->seed()->populateDbFactory();

        $response = $this->json('GET', 'api/stats');

        $response
            ->assertStatus(200)
            ->assertJsonStructure( [
                'graph_questions' => [
                    'question_average' => [
                        '*' => [
                            'question_id',
                            'average_option_value',
                            'chosen',
                            'question_reply_total_count',
                        ]
                    ],
                    'answers_count',
                    'answers_per_question'
                ],
                'open_question' => [
                    'answers_count',
                ] ] );
    }

    /**
     * Api New reply created test case
     */
    public function testUserReplyCreation()
    {
        $this->seed()->populateUsers()->populateQuestions();

        $replies = [];
        $user = DB::table( 'users' )->orderBy( 'id', 'desc' )->first();
        $questions = Question::all();
        $openQuestionModelKeys = Question::getOpenQuestionModelKeys();
        $this->assertIsArray($openQuestionModelKeys);
        $this->assertCount( 1, $openQuestionModelKeys );
        foreach ( $questions as $question ) {
            $replyOption = $question->questionReplyOption()->get()->random();
            $optionValue = $replyOption->option_value;
            if ( in_array( $question->id, $openQuestionModelKeys, true ) ) {
                $optionValue = $this->faker->text();
            }
            $replies[] = [
                'question_reply_option_id' => $replyOption->id,
                'value' => $optionValue,
            ];
        }
        $this->assertCount( 10, $replies );
        $data = [
            'user_id' => $user->id,
            'replies' => $replies,
        ];

        $response = $this->json( 'POST', 'api/questions', $data );
        $response->assertCreated()
            ->assertJson( [ 'result' => 'Reply is created' ] );
    }

    /**
     * Response status and structure test
     */
    public function testInterviewScopeResponse()
    {
        $this->seed()->populateDbFactory();

        $response = $this->json( 'GET', 'api/questions' );

        $response
            ->assertStatus( 200 )
            ->assertJsonStructure( [
                'data'                                      => [
                    '*'                                     => [
                        'interview_id',
                        'interview_uid',
                        'user'                              => [
                            '*'                             => [
                                'user_id',
                                'name',
                                'email',
                            ]
                        ],
                        'user_replies'                      => [
                            '*'                             => [
                                'reply_id',
                                'value',
                                'created',
                                'reply_option'              => [
                                    '*'                     => [
                                        'reply_option_id',
                                        'option_value',
                                        'question'          => [
                                            '*'             => [
                                                'question_id',
                                                'question',
                                                'question_type',
                                            ],
                                        ],
                                    ]
                                ],
                            ]
                        ],
                    ],
                ]

            ] );
    }
}

